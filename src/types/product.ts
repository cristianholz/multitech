export type Product = {
  id: string;
  name: string;
  image_url: string;
};
