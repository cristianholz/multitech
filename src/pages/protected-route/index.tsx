import React from 'react';

import { Navigate, Outlet } from 'react-router-dom';
import { ProtectedRouteType } from './types';

const useAuth = () => {
  let user: any;

  const getUser = localStorage.getItem('@token');

  if (getUser) {
    user = JSON.parse(getUser);
  }
  if (user) {
    return {
      auth: true,
      role: user.role,
    };
  }
  return {
    auth: false,
    role: null,
  };
};

const ProtectedRoutes = (props: ProtectedRouteType) => {
  const { roleRequired } = props;
  const { auth, role } = useAuth();

  if (roleRequired) {
    // eslint-disable-next-line no-nested-ternary
    return auth ? (
      roleRequired === role ? (
        <Outlet />
      ) : (
        <Navigate to="/" />
      )
    ) : (
      <Navigate to="/" />
    );
  }
  return auth ? <Outlet /> : <Navigate to="/" />;
};

export default ProtectedRoutes;
