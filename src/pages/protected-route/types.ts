export type ProtectedRouteType = {
  roleRequired?: 'ADMIN' | 'USER';
};
