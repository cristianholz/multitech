import styled from 'styled-components';

export const Container = styled.div`
  h2 {
    font-size: 28px;
    font-weight: 700;
    margin-top: 30px;
    margin-bottom: 10px;
  }

  .grid {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 4fr));
    grid-gap: 15px;
    margin: 10px 0 30px;
  }
`;
