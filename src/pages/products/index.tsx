import React, { useEffect, useState } from 'react';
import axios from 'axios/axios';
import { Header } from 'components/header';
import { LogoFooter } from 'components/logo-footer';
import { Row } from 'components/row';
import { Spinner } from 'components/spinner';
import { ProductItem } from 'components/product-item';
import { Product } from 'types/product';
import { Container } from './styles';

export function Products(): React.ReactElement {
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setLoading(true);
    axios.get('/products').then((response) => {
      setProducts(response.data);
      setLoading(false);
    });
  }, []);

  function renderProducts() {
    return products.map(({ id, name, image_url }) => (
      <ProductItem key={id} product={{ id, name, image_url }} />
    ));
  }

  return (
    <Container>
      <Row maxWidth="1280px">
        <Header />
        <h2>Produtos</h2>
        <div className="grid">{loading ? <Spinner /> : renderProducts()}</div>
      </Row>
      {loading ? null : <LogoFooter position="relative" />}
    </Container>
  );
}
