import React from 'react';

import { Navigate, Outlet } from 'react-router-dom';

const useAuth = () => {
  const user = localStorage.getItem('@token');
  if (user) {
    return true;
  }
  return false;
};

const PublicRoutes = () => {
  const auth = useAuth();

  return auth ? <Navigate to="/products" /> : <Outlet />;
};

export default PublicRoutes;
