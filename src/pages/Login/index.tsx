import React from 'react';
import { LoginForm } from 'components/login-form';
import { LogoFooter } from 'components/logo-footer';
import { Row } from 'components/row';
import { Container } from './styles';

export function Login(): React.ReactElement {
  return (
    <Container>
      <div className="bottom-cloud">
        <img src="/images/bottom-cloud.svg" alt="Bottom Cloud" />
      </div>
      <div className="top-cloud">
        <img src="/images/top-cloud.svg" alt="Top Cloud" />
      </div>
      <Row maxWidth="500px">
        <h1>
          <img src="/images/logotipo.jpg" alt="Multitech" />
        </h1>
        <LoginForm />
      </Row>
      <LogoFooter />
    </Container>
  );
}
