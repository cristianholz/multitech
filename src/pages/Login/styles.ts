import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  background: ${(props) => props.theme.grey200};
  overflow: hidden;

  h1 {
    text-align: center;
    margin-bottom: 20px;

    img {
      margin: 0 auto;
      max-width: 270px;

      @media screen and (max-width: 1023px) {
        max-width: 200px;
      }
    }
  }

  .bottom-cloud,
  .top-cloud {
    position: fixed;
  }

  .bottom-cloud {
    bottom: -47vw;
    left: -40vw;
    max-width: 75%;

    @media screen and (max-width: 1023px) {
      max-width: 70%;
    }
  }

  .top-cloud {
    top: -15vw;
    right: -16vw;
    max-width: 34%;

    @media screen and (max-width: 1023px) {
      max-width: 40%;
    }
  }
`;
