import { ReactNode } from 'react';

export type ErrorProps = {
  children: ReactNode;
  className?: string;
  textCenter?: boolean;
  fontSize?: string;
};
