import styled from 'styled-components';

export const Container = styled.div<{
  textCenter?: boolean;
  fontSize?: string;
}>`
  color: ${(props) => props.theme.errorColor};
  position: relative;
  display: block;
  font-size: ${(props) => (props.fontSize ? props.fontSize : '12px')};
  margin-top: -13px;
  padding-left: 27px;
  margin-bottom: 12px;
  text-align: ${(props) => (props.textCenter ? 'center' : 'left')};
`;
