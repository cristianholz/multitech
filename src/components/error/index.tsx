import React from 'react';
import { Container } from './styles';
import { ErrorProps } from './types';

export function Error({
  children,
  className,
  textCenter,
  fontSize,
}: ErrorProps): React.ReactElement {
  return (
    <Container
      textCenter={textCenter}
      className={className}
      fontSize={fontSize}
    >
      {children}
    </Container>
  );
}
