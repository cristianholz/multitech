import React from 'react';
import { Container } from './styles';
import { RowProps } from './types';

export function Row({
  children,
  maxWidth,
  className,
}: RowProps): React.ReactElement {
  return (
    <Container className={className} maxWidth={maxWidth}>
      {children}
    </Container>
  );
}
