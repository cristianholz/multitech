import { ReactNode } from 'react';

export type RowProps = {
  children: ReactNode;
  maxWidth?: string;
  className?: string;
};
