import styled from 'styled-components';

export const Container = styled.div<{ maxWidth?: string }>`
  max-width: ${(props) => (props.maxWidth ? props.maxWidth : '1280px')};
  margin: 0 auto;
  width: 100%;
  padding: 0 20px;
`;
