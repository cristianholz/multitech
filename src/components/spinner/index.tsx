import React from 'react';
import { Container } from './styles';

export function Spinner(): React.ReactElement {
  return (
    <Container>
      <div className="spinner" />
    </Container>
  );
}
