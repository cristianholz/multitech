import styled from 'styled-components';

export const Container = styled.div`
  position: fixed;
  top: calc(50% - 40px);
  left: calc(50% - 40px);

  .spinner {
    border: 10px solid ${(props) => props.theme.grey200};
    border-top: 10px solid ${(props) => props.theme.blue};
    border-radius: 50%;
    width: 80px;
    height: 80px;
    animation: spin 1s linear infinite;
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
