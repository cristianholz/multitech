import { Product } from 'types/product';

export type ProductItemProps = {
  className?: string;
  product: Product;
};
