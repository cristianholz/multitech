import { Button } from 'components/button';
import { ButtonVariants } from 'components/button/types';
import React from 'react';
import { Link } from 'react-router-dom';
import { Container } from './styles';
import { ProductItemProps } from './types';

export function ProductItem({
  className,
  product,
}: ProductItemProps): React.ReactElement {
  return (
    <Container className={className}>
      <div className="column">
        <Link to="/products" title={product.name}>
          <figure>
            <img src={product.image_url} alt={product.name} />
          </figure>
          <span className="product-name">{product.name}</span>
          <Button type="button" variant={ButtonVariants.outlined} fullWidth>
            Comprar
          </Button>
        </Link>
      </div>
    </Container>
  );
}
