import styled from 'styled-components';

export const Container = styled.div`
  .column {
    transition: box-shadow 0.2s ease;
    border: 1px solid ${({ theme }) => theme.lineColor};
    padding: 15px;
    display: grid;
    height: 100%;
    border-radius: 8px;

    :hover {
      box-shadow: 2px 3px 7px 2px rgb(0, 0, 0, 0.1);
    }
  }

  a {
    display: grid;
    grid-template-rows: 1fr;
  }

  figure {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  img {
    max-height: 350px;
  }

  .product-name {
    color: ${(props) => props.theme.grey};
    margin-top: 5px;
    font-weight: 900;
  }

  button {
    margin-top: 20px;
  }
`;
