import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { User } from 'types/user';
import { Container } from './styles';
import { HeaderProps } from './types';

export function Header({ className }: HeaderProps): React.ReactElement {
  const [userName] = useState<User>(() => {
    const localName = localStorage.getItem('@token') || '';
    const initialValue = JSON.parse(localName);
    return initialValue || '';
  });

  const history = useNavigate();

  function handleLogout() {
    localStorage.removeItem('@token');
    history('/');
  }

  return (
    <Container className={className}>
      <h1>
        <img src="/images/multitech-logo.svg" alt="Logotipo Multilaser" />
      </h1>
      <div className="container-login">
        <div className="container-user">
          <span>Bem vindo,</span>
          <span>{userName?.name}</span>
        </div>
        <button type="button" onClick={handleLogout}>
          Sair
        </button>
      </div>
    </Container>
  );
}
