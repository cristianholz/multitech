import styled from 'styled-components';

export const Container = styled.div<{ maxWidth?: string }>`
  width: 100%;
  background: ${(props) => props.theme.white};
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px 0;
  flex-wrap: wrap;

  @media screen and (max-width: 490px) {
    padding: 8px 0;
    justify-content: center;
  }

  h1 {
    img {
      min-width: 200px;
    }
  }

  .container-login {
    display: flex;
    align-items: center;

    .container-user {
      padding-right: 10px;

      span {
        margin: 0 2px;
      }
    }
  }

  button {
    background: none;
  }
`;
