import React from 'react';
import { StyledButton } from './styles';
import { ButtonProps } from './types';

export function Button({
  children,
  className,
  fullWidth,
  isLoading,
  variant,
}: ButtonProps): React.ReactElement {
  return (
    <StyledButton
      fullWidth={fullWidth}
      className={`${className} ${variant}`}
      disabled={isLoading}
    >
      {isLoading ? 'Enviando...' : children}
    </StyledButton>
  );
}
