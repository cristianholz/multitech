import { ButtonHTMLAttributes, ReactNode } from 'react';

export enum ButtonVariants {
  primary = 'primary',
  secondary = 'secondary',
  outlined = 'outlined',
}

export type ButtonProps = {
  children: ReactNode;
  className?: string;
  fullWidth?: boolean;
  isLoading?: boolean;
  variant?: ButtonVariants;
} & ButtonHTMLAttributes<HTMLButtonElement>;
