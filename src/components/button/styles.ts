import styled from 'styled-components';

export const StyledButton = styled.button<{ fullWidth?: boolean }>`
  width: ${(props) => (props.fullWidth ? '100%' : 'auto')};
  padding: 10px 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => props.theme.blue};
  color: ${(props) => props.theme.white};
  border-radius: 42px;
  min-height: 48px;
  font-weight: 700;
  font-size: 18px;
  transition: all 0.2s ease-out;

  @media screen and (max-width: 1023px) {
    font-size: 14px;
    min-height: 35px;
  }

  &:hover {
    filter: brightness(85%);
  }

  &:disabled {
    cursor: not-allowed;
    filter: brightness(85%);
  }

  &.outlined {
    background: transparent;
    color: ${(props) => props.theme.blue};
    border: 1px solid ${(props) => props.theme.blue};

    :hover {
      filter: none;
      background: ${(props) => props.theme.blue};
      color: ${(props) => props.theme.white};
    }
  }
`;
