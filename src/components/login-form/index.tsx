import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { User } from 'types/user';
import { FaLock, FaUserAlt } from 'react-icons/fa';
import axios from '../../axios/axios';
import { Button } from '../button';
import { Input } from '../input';
import { Container } from './styles';
import { FormFields, LoginFormProps } from './types';
import { Error } from '../error';

const PERMISSION_TYPE = 'ADMIN';

const formErrorMessages = {
  user: 'Utilize seu usuário de rede',
  password: 'Utilize sua senha de rede',
  usernameOrPasswordIsInvalid: 'Usuário ou senha inválidos!',
};

export function LoginForm({ className }: LoginFormProps): React.ReactElement {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormFields>();
  const [hasFormError, setHasFormError] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const history = useNavigate();

  function setUserInLocalstorage(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  function renderError(message: string) {
    if (hasFormError) {
      return (
        <Error textCenter fontSize="14px">
          {message}
        </Error>
      );
    }
    return null;
  }

  function onSubmit(data: FormFields): void {
    const { user, userPassword } = data;

    setIsLoading(true);

    axios
      .get(`/authentication`)
      .then((response) => {
        const userData: User = response.data.find(
          ({ id }: User) => id === user,
        );

        const { id, name } = userData;

        if (userData) {
          if (userPassword !== name) {
            setHasFormError(true);
            setIsLoading(false);
          } else {
            setHasFormError(false);
            setIsLoading(false);
            setUserInLocalstorage(
              '@token',
              JSON.stringify({ id, name, role: PERMISSION_TYPE }),
            );

            history('/products');
            reset();
          }
        } else {
          setHasFormError(true);
          setIsLoading(false);
        }
      })
      .catch(() => {
        setHasFormError(true);
        setIsLoading(false);
      });
  }

  return (
    <Container className={className}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          {renderError(formErrorMessages.usernameOrPasswordIsInvalid)}
        </div>
        <div className="form-group">
          <Input
            icon={FaUserAlt}
            id="user"
            type="text"
            label="Usuário da rede"
            error={errors.user ? 'error' : ''}
            register={register('user', {
              required: {
                value: true,
                message: formErrorMessages.user,
              },
            })}
          />
        </div>
        {errors.user ? <Error>{errors.user.message}</Error> : null}
        <div className="form-group">
          <Input
            icon={FaLock}
            inputPassword
            id="password"
            label="Senha de rede"
            error={errors.userPassword ? 'error' : ''}
            register={register('userPassword', {
              required: {
                value: true,
                message: formErrorMessages.password,
              },
            })}
          />
        </div>
        {errors.userPassword ? (
          <Error>{errors.userPassword.message}</Error>
        ) : null}

        <div className="form-group">
          <Button
            className="button-submit"
            fullWidth
            type="submit"
            isLoading={isLoading}
          >
            Login
          </Button>
        </div>
        <span className="observation">
          * Utilize o seu usuário e senha da rede para fazer login
        </span>
      </form>
    </Container>
  );
}
