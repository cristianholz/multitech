import styled from 'styled-components';

export const Container = styled.div<{ maxWidth?: string }>`
  .form-group {
    margin-bottom: 20px;
  }

  .observation {
    width: 100%;
    text-align: center;
    color: ${(props) => props.theme.grey};
    display: block;
    margin-top: 40px;
    font-size: 12px;
  }

  button.button-submit {
    min-height: 52px;
  }
`;
