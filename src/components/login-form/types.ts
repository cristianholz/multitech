export type LoginFormProps = {
  className?: string;
};

export type FormFields = {
  user: string;
  userPassword: string;
};
