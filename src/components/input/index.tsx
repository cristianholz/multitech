import React, { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';
import { Container } from './styles';
import { InputProps } from './types';

export function Input({
  className,
  id,
  name,
  label,
  type,
  inputPassword,
  icon: Icon,
  iconSize,
  register,
  error,
}: InputProps): React.ReactElement {
  const [passwordShown, setPasswordShown] = useState(false);

  function togglePassword() {
    setPasswordShown(!passwordShown);
  }

  function renderLabel() {
    if (!label) return null;

    return <label htmlFor={id}>{label}</label>;
  }

  function renderInput() {
    if (!inputPassword) {
      return (
        <input
          name={name}
          id={id}
          className="input"
          type={type}
          {...register}
        />
      );
    }

    return null;
  }

  function renderInputPassword() {
    if (inputPassword) {
      return (
        <div className="container-input-password">
          <input
            name={name}
            id={id}
            className="input-password"
            type={passwordShown ? 'text' : 'password'}
            autoComplete="on"
            {...register}
          />
          <button
            type="button"
            onClick={togglePassword}
            className="toggle-password"
          >
            {passwordShown ? <FaEyeSlash /> : <FaEye />}
          </button>
        </div>
      );
    }

    return null;
  }

  function renderIcon() {
    if (!Icon) return null;

    return (
      <i>
        <Icon size={iconSize || '18'} />
      </i>
    );
  }

  return (
    <Container className={className}>
      <div className={`input-background ${error}`}>
        {renderIcon()}
        <div className="input-container">
          {renderLabel()}
          {renderInput()}
          {renderInputPassword()}
        </div>
      </div>
    </Container>
  );
}
