import styled from 'styled-components';

export const Container = styled.div<{ maxWidth?: string }>`
  .input-background {
    padding: 12px 20px 8px;
    border: 1px solid transparent;
    background: ${(props) => props.theme.white};
    border-radius: 42px;
    font-size: 14px;
    display: flex;
    align-items: center;
  }

  .input-container {
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 0 10px;
    position: relative;
  }

  i {
    color: ${(props) => props.theme.blue};
    margin-right: 10px;
  }

  label {
    display: block;
    width: 100%;
    color: ${(props) => props.theme.grey};
  }

  input {
    background: none;
    border: none;
    width: 100%;
    padding: 5px 25px 4px 0;
    color: ${(props) => props.theme.dark};

    :-webkit-autofill,
    :-webkit-autofill:focus {
      transition: background-color 600000s 0s, color 600000s 0s;
    }
  }

  input[data-autocompleted] {
    background-color: transparent;
  }

  .container-input-password {
    display: flex;
    align-items: center;

    .toggle-password {
      position: absolute;
      right: 0;
      top: 50%;
      transform: translateY(-10px);
      color: ${(props) => props.theme.blue};
      background: none;

      svg {
        font-size: 20px;
        color: ${(props) => props.theme.grey};
      }
    }
  }

  .error {
    border-color: ${(props) => props.theme.errorColor};

    label {
      color: ${(props) => props.theme.errorColor};
    }
  }
`;
