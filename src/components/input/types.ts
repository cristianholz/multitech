import { InputHTMLAttributes, ComponentType } from 'react';
import { UseFormRegisterReturn } from 'react-hook-form';
import { IconBaseProps } from 'react-icons';

export type InputProps = {
  className?: string;
  label?: string;
  inputPassword?: boolean;
  register?: UseFormRegisterReturn;
  error?: string;
  icon?: ComponentType<IconBaseProps>;
  iconSize?: string | number;
} & InputHTMLAttributes<HTMLInputElement>;
