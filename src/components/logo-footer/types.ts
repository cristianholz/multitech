export type LogoFooterProps = {
  className?: string;
  position?: string;
};
