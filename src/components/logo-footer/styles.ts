import styled from 'styled-components';

export const Container = styled.div<{ position?: string }>`
  display: flex;
  align-items: center;
  justify-content: center;
  bottom: 0;
  position: ${(props) => (props.position ? props.position : 'absolute')};
  min-height: 105px;
  width: 100%;

  img {
    max-width: 130px;
  }
`;
