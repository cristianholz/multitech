import React from 'react';
import { Container } from './styles';
import { LogoFooterProps } from './types';

export function LogoFooter({
  className,
  position,
}: LogoFooterProps): React.ReactElement {
  return (
    <Container className={className} position={position}>
      <img src="/images/multitech-logo.svg" alt="Logotipo Multitech" />
      <img src="/images/logotipo-multilaser.svg" alt="Logotipo Multilaser" />
    </Container>
  );
}
