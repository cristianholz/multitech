import { createGlobalStyle } from 'styled-components';
import 'normalize.css/normalize.css';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  html, body, #root{
    height: 100%;
    overflow-x: hidden;
  }
  

  body, input, textarea, button {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 400;
  }

	figure, img, svg, video {
		max-width: 100%;
	}

	figure {
		width: auto;
	}

	video {
		display: block;
		width: 100%;
	}

  a {
    color: inherit;
    text-decoration: none;
  }

  button {
    border: 0;
    cursor: pointer;
  }

	@media (prefers-reduced-motion: reduce) {
		*,
		*::before,
		*::after {
			animation-duration: 0.001ms;
			animation-iteration-count: 1;
			transition-duration: 0.001ms;
		}
	}

	.hide:not(:focus):not(:active),
	.hidden:not(:focus):not(:active) {
		clip: rect(0 0 0 0);
		clip-path: inset(50%);
		height: 1px;
		overflow: hidden;
		position: absolute;
		white-space: nowrap;
		width: 1px;
  }
`;
