export interface DefaultThemeProps {
  blue: string;
  dark: string;
  light: string;
  grey: string;
  grey200: string;
  lineColor: string;
  errorColor: string;
  white: string;
}
