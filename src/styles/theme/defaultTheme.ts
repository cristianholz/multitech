import baseStyled, { ThemedStyledInterface } from 'styled-components';

export const theme = {
  blue: '#009ed2',
  dark: '#000',
  light: '#fff',
  grey: '#656667',
  grey200: '#e9ebea',
  lineColor: '#e5e5e5',
  errorColor: '#c10a16',
  white: '#fff',
};

export type Theme = typeof theme;
export const styled = baseStyled as ThemedStyledInterface<Theme>;
