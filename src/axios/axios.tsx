import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://62681ad33f45bffa83879c74.mockapi.io/api/v1',
});
export default instance;
