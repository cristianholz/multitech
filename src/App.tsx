import React from 'react';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Login } from 'pages/Login';
import { theme } from 'styles/theme/defaultTheme';
import GlobalStyles from 'styles/GlobalStyles';
import { Products } from 'pages/products';
import ProtectedRoutes from 'pages/protected-route';
import PublicRoutes from 'pages/public-route';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/products" element={<ProtectedRoutes />}>
            <Route path="/products" element={<Products />} />
          </Route>
          <Route path="/" element={<PublicRoutes />}>
            <Route path="/" element={<Login />} />
            <Route path="*" element={<Login />} />
          </Route>
        </Routes>
      </BrowserRouter>
      <GlobalStyles />
    </ThemeProvider>
  );
};

export default App;
